Rule format: 

	SRCIP DIRECTION DSTIP PROTO/PORT

Example rules:

	192.168.1.1 > 10.10.10.10 tcp/22
	192.168.1.1 - 10.10.10.10 tcp/22
	* > 8.8.8.8 udp/53

SRC and DST IP Address can be: 
	
	A single IP address, or * 

DIRECTION:

	< (DST to SRC)
	- (SRC and DST)
	> (SRC to DST)

PROTO: 

	udp, tcp

PORT: 

	A valid port number
