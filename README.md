Intruding PI 
=========

Intruding PI  (IPI) is a lightweight passive IDS. 
Designed for the Raspberry PI, but suitable for large scale operations. 

Dependencies

- [meson](http://mesonbuild.com/) For building and dependency checking.
- [PF_RING](https://github.com/ntop/PF_RING) For packet capture and filtering.
- [nDPI](https://github.com/ntop/nDPI) For deep packet inspection and Layer 7.

Create development environment: 

	vagrant up

To build and perform basic static analysis:

	meson build   						# Check for dependencies and create build script
	ninja -C build scan-build 	# Perform a static analysis of the code
	ninja -C build 						# Build exe ./build/ipi
