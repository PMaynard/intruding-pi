// utils.c
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

static char hex[] = "0123456789ABCDEF";
char* etheraddr_string(const u_char *ep, char *buf) {
  u_int i, j;
  char *cp;

  cp = buf;
  if((j = *ep >> 4) != 0)
    *cp++ = hex[j];
  else
    *cp++ = '0';

  *cp++ = hex[*ep++ & 0xf];

  for(i = 5; (int)--i >= 0;) {
    *cp++ = ':';
    if((j = *ep >> 4) != 0)
      *cp++ = hex[j];
    else
      *cp++ = '0';

    *cp++ = hex[*ep++ & 0xf];
  }

  *cp = '\0';
  return (buf);
}

char *ip_str(u_int32_t ip) {
  char buffer[INET_ADDRSTRLEN];
  char *d = malloc(INET_ADDRSTRLEN);
  if(d == NULL){
    return NULL;
  }
  ip = htonl(ip);

  const char* result=inet_ntop(AF_INET, &ip, buffer, sizeof(buffer));
  if (result==0) {
    printf("Unable to convert IP address\n");
    free(d);
    return NULL;
  }
  strncpy(d, result, INET_ADDRSTRLEN);
  return d; 
}


// char *strdup (const char *s) {
//     char *d = malloc (strlen (s) + 1);   // Space for length plus nul
//     if (d == NULL) return NULL;          // No memory
//     strcpy (d,s);                        // Copy the characters
//     return d;                            // Return the new string
// }