#include <stdio.h>

#include "config.h"
#include "rules.h"
#include "debug.h"
#include "utils.h"
#include "init.h"
#include "log.h"

int main()
{
	if(loadRules() != 0){
		printf("Unable to load rules, exiting!\n");
		return -1;
	}
	// logalert("10.50.50.1 > 8.8.8.8 udp/53");
	print_debug();
	if(init() != 0) {
		printf("%s\n", "Unable to initialise, exiting!");
		return -1;
	}
	return 0;
}