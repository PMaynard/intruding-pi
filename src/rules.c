// rules.c

#include <stdio.h>
#include <string.h>

#include "config.h"
#include "rules.h"
#include "utils.h"

int loadRules(){
	FILE* valid_rules;
	char str_rule[RULE_LEN];

	valid_rules = fopen(VALID_RULES_FILE, "r");
	if(valid_rules == NULL){
		printf("Error opening rules file: %s\n", VALID_RULES_FILE);
		return -1;
	} 

	while(fgets(str_rule, RULE_LEN, valid_rules) != NULL){
		if(strlen(str_rule) < 7){
			// TODO: Use validRule.
			continue;	
		} 

		struct rule r1;
		if(parseRule(str_rule, r1) != 0){
			return -1;
		}
	}

	fclose(valid_rules);
	return 0;
}


int validRule(const char* r){
	// TODO: 
	return 0;
}

int parseRule(const char* r_str, struct rule r) {
	// Validity Check of rule.
	// if (validRule(r_str) == 0){
	// 	printf("%s\t%s\n", TICK, r_str);		
	// }else{
	// 	printf("%s\t%s\n", CROSS, r_str);
	// 	return -1;
	// }

	// variables for parsing.
	// TODO: Calculate the correct sizes for these.
	char srcip[100];
	// TODO: Change dir to read as a char.
	char dir[2];
	char dstip[100];
	char proto[4];
	int port;

	// Extract values.
	if(sscanf(r_str, "%99s %3s %99s %3s/%d", srcip, dir, dstip, proto, &port)!=5){
		printf("Error Parsing rule: %s \n", r_str);
		return -1;
	}

	printf("READ: %s %s %s %s/%d\n", srcip, dir, dstip, proto, port);

	r.srcip = srcip;
	r.dir = parseDir(dir);
	r.dstip = dstip;
	r.proto = parseProto(proto);
	r.port = port;
	printRule(r);

	return 0;
}

// Parse the direction from char (e.g. <,-,>) to an enum. Returns -1 if error.
enum DIR parseDir(const char* dir) {
	switch(dir[0]){
		case '<': return SRC;
		break;
		case '-': return ANY;
		break;
		case '>': return DST;
	}
	return -1;
}

// Parse the proto from char (e.g. tcp,udp) to an enum. Returns -1 if error.
enum PROTO parseProto(const char* proto) {
	if(strncmp(proto, "tcp", 3) == 0){
		return TCP;
	}

	if(strncmp(proto, "udp", 3) == 0){
		return UDP;
	}

	return -1;
}

// Prints a human friendly rule.
void printRule(struct rule r) {
	char* dir;
	char* proto;

	switch(r.dir){
		case SRC: dir = "<";
		break;
		case ANY: dir = "-";
		break;
		case DST: dir = ">";
		break;
	}

	switch(r.proto) {
		case TCP: proto = "tcp";
		break;
		case UDP: proto = "udp";
		break;
	}

	printf("%s %s %s %s/%d\n", r.srcip, dir, r.dstip, proto, r.port);
}