// User configuration.
#define DEFAULT_DEVICE "lo"
#define BFP_FILTER "not port 22"
#define VALID_RULES_FILE "/vagrant/rules/valid.rules"
#define SYSLOG_NAME "IPI"

// Not required to change.
#define ALARM_SLEEP 3
#define DEFAULT_SNAPLEN 1536
#define NO_ZC_BUFFER_LEN 9000
#define RULE_LEN 100