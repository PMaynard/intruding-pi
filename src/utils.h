#ifndef FUNCTION_UTILS_INCLUDED
#define FUNCTION_UTILS_INCLUDED

#include <arpa/inet.h>

#define TICK "\u2713"
#define CROSS "\u274C"

char* etheraddr_string(const u_char *ep, char *buf);
char* ip_str(u_int32_t ip);

#endif