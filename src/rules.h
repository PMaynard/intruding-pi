// rules.h

#ifndef FUNCTION_RULES_INCLUDED
#define FUNCTION_RULES_INCLUDED
enum PROTO {
	TCP,
	UDP
};

enum DIR
{
	SRC,
	ANY,
	DST
};

// SRCIP DIRECTION DSTIP PROTO/PORT
struct rule
{
	char* srcip;
	enum DIR dir ;
	char* dstip;
	enum PROTO proto;
	int port;
};

int loadRules();
int validRule(const char* r);
enum DIR parseDir(const char* dir);
enum PROTO parseProto(const char* proto);
int parseRule(const char* r, struct rule buff);
void printRule(struct rule r);

#endif
