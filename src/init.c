// init.c

#include "config.h"
#include "debug.h"
#include "utils.h"
#include "init.h"

#include <arpa/inet.h>

u_int8_t wait_for_packet = 1,  do_shutdown =  0;
int alarm_sleep;

pfring *pd;

void* packet_consumer_thread(void* _id) {
  u_char buffer[NO_ZC_BUFFER_LEN];
  u_char *buffer_p = buffer;

  struct pfring_pkthdr hdr;

  memset(&hdr, 0, sizeof(hdr));

  while(1) {
	int rc;

	if(do_shutdown) break;

	if((rc = pfring_recv_parsed(pd, &buffer_p, NO_ZC_BUFFER_LEN, &hdr, wait_for_packet, 7, 1, 1)) > 0) {
	  if(do_shutdown) break;
	  // 
	  const struct pkt_parsing_info *h = &hdr.extended_hdr.parsed_pkt;

	  char *srcip = ip_str(h->ip_src.v4);
	  char *dstip = ip_str(h->ip_dst.v4);

	  // if(srcip == NULL || dstip == NULL){
	  // 	return(NULL);
	  // }

	  printf("%s ? %s ?/%d\n", srcip, dstip, h->l4_src_port);

	  free(srcip);
	  free(dstip);

	  // 
	} else {
	  if(wait_for_packet == 0) sched_yield();
	}
  }

  return(NULL);
}

// Handle signals
void sigproc(int sig) {
  static int called = 0;
  logmsg(LOG_INFO, "IPI Exiting");
  fprintf(stderr, "Leaving...\n");
  if(called) return; else called = 1;
  do_shutdown = 1;

  print_stats(pd);
  pfring_breakloop(pd);
  //   pfring_close(pd);
}

void my_sigalarm(int sig) {
  if(do_shutdown)
	return;

  alarm(alarm_sleep);
  signal(SIGALRM, my_sigalarm);
}

void enable_bpf(char* bpfFilter){
	int rc = 0;
	if(bpfFilter != NULL) {
		rc = pfring_set_bpf_filter(pd, bpfFilter);
		if(rc != 0) {
			fprintf(stderr, "pfring_set_bpf_filter(%s) returned %d\n", bpfFilter, rc);
		}
		else{
			printf("Successfully set BPF filter '%s'\n", bpfFilter);
		}
	}
}

int init() {
	char *device = NULL;
	char buf[32]; 
	int snaplen = DEFAULT_SNAPLEN;
	u_char mac_address[6] = { 0 };
	u_int32_t flags = 0;

	// TODO: CLI or Config parsing.
	// device = strdup("lo");
	// device = "lo";
	// snaplen = atoi("100"); // Trigger warning.

	// Pick an interface
	if(device == NULL){
		device = DEFAULT_DEVICE;
	}

	// Set the snap length
	 if(snaplen < 1536) {
	  snaplen = 1536;
	  fprintf(stderr, "WARNING: Snaplen smaller than the MTU. Enlarging it (new snaplen %u)\n", snaplen);
	}

	// Set flags
	flags |= PF_RING_PROMISC; // Enable Promisc mode
	flags |= PF_RING_HW_TIMESTAMP; // Enable Hardware timestamp

	// Set direction to monitor
	pfring_set_direction(pd,  rx_only_direction);

	// open
	pd = pfring_open(device, snaplen, flags);
	if(pd == NULL) {
		fprintf(stderr, "pfring_open error [%s] (pf_ring not loaded or interface %s is down ?)\n",
			strerror(errno), device);
		return(-1);
	}

	enable_bpf("src port 22");

	// Print out some version information
	u_int32_t version;
	pfring_set_application_name(pd, "ipi");
	pfring_version(pd, &version);
	logmsg(LOG_INFO, "IPI Started");
	printf("PF_RING v.%d.%d.%d\n", (version & 0xFFFF0000) >> 16,  (version & 0x0000FF00) >> 8, version & 0x000000FF);

	// Print out device name and mac address.
	if(pfring_get_bound_device_address(pd, mac_address) != 0) {
		fprintf(stderr, "Unable to read the device address\n");
		return(-1);
	}

	int ifindex = -1;
	pfring_get_bound_device_ifindex(pd, &ifindex);
	printf("Capturing from %s [%s][ifIndex: %d]\n", device, etheraddr_string(mac_address, buf), ifindex);

// *******************************************

	signal(SIGINT, sigproc);
	signal(SIGTERM, sigproc);
	signal(SIGINT, sigproc);
	signal(SIGALRM, my_sigalarm);
	alarm(ALARM_SLEEP);

	if (pfring_enable_ring(pd) != 0) {
		printf("Unable to enable ring :-(\n");
		pfring_close(pd);
		return(-1);
	}

	pthread_t my_thread;
	pthread_create(&my_thread, NULL, packet_consumer_thread, (void*)0);
	pthread_join(my_thread, NULL);

	sleep(1);
	pfring_close(pd);
	return 0;
}