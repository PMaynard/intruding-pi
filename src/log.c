// log.c

#include "config.h"
#include "log.h"

void logmsg(int facility_priority, const char *s) {
	openlog (SYSLOG_NAME, LOG_CONS, LOG_USER);
	syslog (LOG_MAKEPRI(LOG_USER, facility_priority), "%s", s);
}

void logalert(const char *s) {
	openlog (SYSLOG_NAME, LOG_CONS, LOG_USER);
	syslog (LOG_MAKEPRI(LOG_USER, LOG_ALERT), "%s", s);
}