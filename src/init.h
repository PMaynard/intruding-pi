#ifndef FUNCTION_INIT_INCLUDED
#define FUNCTION_INIT_INCLUDED

#include <pcap.h>
#include <signal.h>
#include <arpa/inet.h>

#include "pfring.h"

void enable_bpf(char* bpfFilter);
void sigproc(int sig);
void my_sigalarm(int sig);
int init();

#endif