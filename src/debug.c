// debug.c

#include "debug.h"

void printDevs() {
	int i = 0;
	pfring_if_t *dev;

	dev = pfring_findalldevs();

	printf("Available devices:\n");

	while (dev != NULL) {
		printf(" %d. %s\n", i++, dev->name);
		dev = dev->next;
	}
}

void print_debug() {	
	printf("MAX_CAPLEN:\t%d\n", MAX_CAPLEN);	
	printf("PAGE_SIZE:\t%d\n", PAGE_SIZE);	
	printf("DEFAULT_POLL_DURATION:\t%d\n", DEFAULT_POLL_DURATION);	
	printf("POLL_SLEEP_STEP:\t%d\n", POLL_SLEEP_STEP);	
	printf("POLL_SLEEP_MIN:\t%d\n", POLL_SLEEP_MIN);	
	printf("POLL_SLEEP_MAX:\t%d\n", POLL_SLEEP_MAX);	
	printf("POLL_QUEUE_MIN_LEN:\t%d\n", POLL_QUEUE_MIN_LEN);	
	// 
	printf("DEFAULT_DEVICE:\t%s\n", DEFAULT_DEVICE);	
	// 
	printDevs();
}

void print_stats(pfring *pd) {
	pfring_stat stats;

	pfring_stats(pd, &stats);
	printf("RECV: %ld, DROP: %ld\n", (long)stats.recv, (long)stats.drop);
}