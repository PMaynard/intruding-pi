#ifndef FUNCTION_LOG_INCLUDED
#define FUNCTION_LOG_INCLUDED

#include <syslog.h>
#include <stdio.h>

void logmsg(int facility_priority, const char *s);
void logalert(const char *s);

#endif
