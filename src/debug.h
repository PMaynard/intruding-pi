#ifndef FUNCTION_DEBUG_INCLUDED
#define FUNCTION_DEBUG_INCLUDED

#include "config.h"
#include "log.h"

#include "pfring.h"

void printDevs();
void print_debug();
void print_stats(pfring *pd);

#endif